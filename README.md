# README #

### What is this repository for? ###

* This repository holds utilities (Classes, Prefabs, etc.) for Unity3D to make creating games easier and faster.

### How do I get set up? ###

* If you want to use the Utils for yourself you need to create your own Unity3D Project and import the Utils folder there.
* If you want to work with git, you first have to create/clone the repository and then create the unity project inside that folder.
* If you want to contribute make shure before you first commit anything you force text serialization inside Unity3D (Project Settings -> Editor) or you'll break the prefabs and scenes.

### Contribution guidelines ###

* Try to make your contributions as **modular** as possible.
* **Document** your contributions (or noone else will know what they do and how to use them).
* **Do not commit untested** or **broken** code to the master branch, use the development branch instead or create your own.
* Write **meaningfull** (or at least funny) **commit messages**.
* **Copy** the license text from **LICENSE.txt** in the root folder and paste it **at the top of your code**.
* Ensure the **right authors are listed in the license text** to prevent support requests to the wrong authors from happening.
* Use the bottom of the license text to **give credits to the authors you based your code on**.
* Unchanged code belongs into the *3rd Party* folder