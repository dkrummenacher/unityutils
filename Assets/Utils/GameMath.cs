﻿using UnityEngine;

namespace Utils {

	/// <summary>
	/// Helper methods for game math stuff.
	/// </summary>
	public abstract class GameMath {
		
		/// <summary>
		/// Get the result of a dice roll.
		/// </summary>
		/// <returns>The result of the roll.</returns>
		/// <param name="amountOfDices">Amount of dices.</param>
		/// <param name="sides">Sides per dice.</param>
		public static int DiceRoll(int amountOfDices = 1, int sides = 6) {
			if (sides < 2) {
				Debug.LogWarning("Dice cannot have less than 2 sides. Returning 1.");
				return 1;
			}
			
			int result = 0;
			for (int i = 0; i < amountOfDices; i++) result += Random.Range(0, sides) + 1;
			return result;
		}
		
		/// <summary>
		/// Returns wether a something is true or false based on a chance expressed in percent.
		/// This function is accurate to two decimal places (eg. 99.99%).
		/// </summary>
		/// <returns>The result of the roll.</returns>
		/// <param name="percent">Percent of chance.</param>
		/// <param name="fraction">If set to <c>true</c> uses fractions instead of full values (eg. 0.5 instead of 50%).</param>
		public static bool Chance(float percent, bool fraction = false) {
			return (fraction) ? Random.Range(0, 10000) < percent * 10000f : Random.Range(0, 10000) < percent * 100f;
		}
		
		/// <summary>
		/// Returns the percent based on percentage and base value.
		/// </summary>
		/// <returns>The percent.</returns>
		/// <param name="percentage">Percentage value.</param>
		/// <param name="baseValue">Base value.</param>
		/// <param name="fraction">If set to <c>true</c> uses fractions instead of full values (eg. 0.5 instead of 50%).</param>
		public static float Percent(float percentage, float baseValue, bool fraction = false) {
			return (fraction) ? percentage / baseValue : percentage / baseValue * 100f;
		}
		
		/// <summary>
		/// Returns the percentage value based on percent and base value.
		/// </summary>
		/// <returns>The percentage value.</returns>
		/// <param name="baseValue">Base value.</param>
		/// <param name="percent">Percent.</param>
		/// <param name="fraction">If set to <c>true</c> uses fractions instead of full values (eg. 0.5 instead of 50%).</param>
		public static float PercentPercentage(float baseValue, float percent, bool fraction = false) {
			return (fraction) ? baseValue * percent : baseValue * percent / 100f;
		}
		
		/// <summary>
		/// Returns the base value based on percentage value and percent.
		/// </summary>
		/// <returns>The base value.</returns>
		/// <param name="percentage">Percentage value.</param>
		/// <param name="percent">Percent.</param>
		/// <param name="fraction">If set to <c>true</c> uses fractions instead of full values (eg. 0.5 instead of 50%).</param>
		public static float PercentBaseValue(float percentage, float percent, bool fraction = false) {
			return (fraction) ? percentage / percent : percentage / percent * 100f;
		}
		
		/// <summary>
		/// Returns the digit sum of an integer.
		/// </summary>
		/// <returns>The digit sum.</returns>
		/// <param name="i">The integer.</param>
		public static int DigitSum(int i) {
			int sum = 0;
			for (int n = Mathf.Abs(i); n > 0; sum += n % 10, n /= 10);
			return sum;
		}
	}
}
