﻿/*
 * Code by It3ration
 * (http://answers.unity3d.com/questions/489942/how-to-make-a-readonly-property-in-inspector.html)
 */

using UnityEngine;

/// <summary>
/// Read only attribute.
/// </summary>
public class ReadOnlyAttribute : PropertyAttribute { }
